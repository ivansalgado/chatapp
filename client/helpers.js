Template.welcome.helpers({
    "message": function () {
        return "sign in to begin";
    }
});

Template.posts.helpers({
    "posts": function () {
        return Posts.find({}).fetch();
    }
});